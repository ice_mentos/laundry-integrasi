package com.example.laondry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaondryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LaondryApplication.class, args);
	}

}
