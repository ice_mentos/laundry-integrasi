package com.example.laondry.service;

import com.example.laondry.entity.Admin;
import com.example.laondry.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {
    @Autowired
    private AdminRepository adminRepository;

    public List<Admin> getAdmin(){
        return adminRepository.findAll();
    }

    public Admin getUsername(String username){
        return adminRepository.findByUsername(username);
    }
}
